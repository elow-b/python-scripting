#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import os
import re

def strextract(args):
    regex_dir = re.compile("\"[^\"]*\"|\'[^\']*\'")
    for root, _, filenames in os.walk(args.dir) :
        for filename in filenames:
            file_path = os.path.join(root, filename)
            if args.all or not filename.startswith("."):
                if not args.suffix or filename.endswith(args.suffix) :
                    with open(file_path) as file :
                        for line in file:
                            match_line = re.findall(regex_dir,line)
                            for string in match_line : 
                                if not string:
                                    continue
                                else:
                                    if args.path : 
                                        print(file_path,match_line, sep='\t')
                                    else : 
                                        print(match_line)

def main():
    # build an empty parser
    parser = argparse.ArgumentParser()
    
    # define arguments
    parser.add_argument("dir", type=str, help="directory choose")
    parser.add_argument("-s", "--suffix",  default="", type=str, help="suffix of to be deleted files")
    parser.add_argument("--path", action="store_true", help="path directory for each ")
    parser.add_argument("-a","--all", action="store_true", help="Include all files")
    args = parser.parse_args()

    strextract(args)

if __name__ == '__main__':
    try:
        main()
    except FileNotFoundError:
        print("erreur fichier")