#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
import argparse
from urllib.parse import urljoin



def brklnk(url, depth, visited_urls):
    if depth >= 0 and url not in visited_urls:
        response = requests.get(url)
        if response.status_code >= 400:
            print(url, "Broken link", sep=" --> ")
        elif depth >= 1 : 
            parse_html = BeautifulSoup(response.text, 'html.parser')
            for response in parse_html.find_all('a'):
                join_link = urljoin(url,response.get('href'))
                if join_link not in visited_urls :
                    visited_urls.add(response.get('href'))
                brklnk(join_link, depth-1, visited_urls )  

def main():
    # build an empty parser
    parser = argparse.ArgumentParser()
    
    # define arguments
    parser.add_argument("url", type=str, help="url choose to check broken links")
    parser.add_argument("--depth", default="1", type=int, help="deep search")
    args = parser.parse_args()

    visited_urls = set()
    
    brklnk(args.url,args.depth, visited_urls)

if __name__ == '__main__':
        main()
